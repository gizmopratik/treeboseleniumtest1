package pageFactoryMobile;



import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.AssertJUnit;

import io.selendroid.client.SelendroidDriver;
 


public class SearchPopupMobile {
 
    /**
     * All WebElements are identified by @FindBy annotation
     */
 
	SelendroidDriver driver;

    @FindBy(id="searchInput")
    WebElement searchInput;

    // @FindBy(xpath="//div/div[2]/form/div[1]/ul/li[1]")
    @FindBy(css="#searchModal > div > div.modal-body.search-widget-body > form > div.row.pos-rel > ul > li:nth-child(1)")
    WebElement city_name;
    
    @FindBy(id="searchSubmitBtn")
    WebElement searchSubmitBtn;
     
    public SearchPopupMobile(SelendroidDriver driver){
        this.driver = driver;
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }
 

    /**
     * This POM method will be exposed in test case to login in the application
     * @param strUserName
     * @param strPasword
     * @return
     * @throws InterruptedException 
     */
    
    public void search_popup() throws InterruptedException{
    	
    	// CLick on the Submit search button to get default results
    	searchSubmitBtn.click();
    	Thread.sleep(5000);
    	
    	// click on bangalore which is first results
    	city_name.click();
    	Thread.sleep(2000);
    	
    	// Click on submit button to get the results
    	driver.manage().timeouts().pageLoadTimeout(100, TimeUnit.SECONDS);
    	searchSubmitBtn.click();
    	Thread.sleep(2000);
    	    }
    
}