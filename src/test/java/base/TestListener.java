package base;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;

import base.TestBaseSetUp;

public class TestListener implements ITestListener {

    public void onFinish(ITestContext context) { 
        Iterator<ITestResult> listOfFailedTests = context.getFailedTests().getAllResults().iterator();
        while (listOfFailedTests.hasNext()) {
            ITestResult failedTest = listOfFailedTests.next();
            ITestNGMethod method = failedTest.getMethod();
            
            if (context.getFailedTests().getResults(method).size() > 1) {
                listOfFailedTests.remove();
            } else {
                if (context.getPassedTests().getResults(method).size() > 0) {
                    listOfFailedTests.remove();
                }
            }
        }
    }
    
    @Override
    public void onTestFailure(ITestResult result) {
        try{
            WebDriver driver = DriverManager.getInstance().getDriver();
            
    		System.out.println("Url for failed page is: " + driver.getCurrentUrl());
    		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    		String datePath = new SimpleDateFormat("yyyy.MM.dd").format(new Date());
    		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
    		String fileName = result.getMethod().getMethodName() + timeStamp;
    		try {
    			FileUtils.copyFile(scrFile, new File(String.format("target/FailedTestScreenshots/%s.png", fileName)));
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    		Set<Cookie> cookies = driver.manage().getCookies();
    		System.out.println("************ List of cookies ** Start************");
    		for (Cookie cookie : cookies) {
    			System.out.println(cookie);
    		}
    		System.out.println("************ List of cookies ** End ************");
        }catch (Exception e){
        	e.printStackTrace();
        }
        
    }

    // Following are all the method stubs that you do not have to implement
  
    public void onTestStart(ITestResult result) {
        // TODO Auto-generated method stub
    }
  
    public void onTestSuccess(ITestResult result) {
        // TODO Auto-generated method stub
    }

    public void onTestSkipped(ITestResult result) {
        // TODO Auto-generated method stub
    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        // TODO Auto-generated method stub
    }

    public void onStart(ITestContext context) {
        // TODO Auto-generated method stub
    }
}  // ends the class