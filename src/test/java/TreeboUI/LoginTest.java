package TreeboUI;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import org.testng.annotations.Test;
import PageFactory.LoginPage;
import DataProvider.DataProviderClass;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeTest;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterTest;

public class LoginTest {

  	WebDriver driver;
  	LoginPage objLogin;


  	@BeforeTest()
  	public void setup() {

  		// Chrome Driver
		System.setProperty("webdriver.chrome.driver", "/usr/local/share/chromedriver");
		//System.setProperty("webdriver.chrome.driver", "/Users/ashishmantri/Downloads/chromedriver");

  	    	driver = new ChromeDriver();
  		// driver = new FirefoxDriver();
  		// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

  		driver.get("http://treebohotels.com");
  	//	driver.manage().window().maximize();

  	}

  	/**
  	 * This test go to http://treebohotels.com Verify login page title as
  	 * toashishmantriATgmail.com Login to application
  	 * @throws InterruptedException 
  	 **/

//  	@Parameters({ "username", "password" })
  	@Test(priority = 0)
  	public void test_login()  throws InterruptedException {

  		// Create Login Page object
  		objLogin = new LoginPage(driver);

		// Make sure that user is not Login
    	AssertJUnit.assertEquals(true, driver.findElements(By.name("headerLogout")).size() == 0);  	
  		
  		// login to application
  		try {
			objLogin.loginToTreebo("treebotest@gmail.com", "password");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Make sure that user is Login
		AssertJUnit.assertEquals(true, driver.findElements(By.name("headerLogout")).size() != 0);

  	}
  	
  	@AfterTest()
  	// Close browser session
  	public void teardown() {
  	//	driver.close();
  	}
  }
