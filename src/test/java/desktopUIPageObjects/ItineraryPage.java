package desktopUIPageObjects;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import utils.CommonUtils;
import base.BrowserHelper;
import base.DriverManager;
import desktopUIPageObjects.HomePage;

public class ItineraryPage {
	private BrowserHelper browserHelper;
	private HomePage homePage;

	private By itineraryPage = By.xpath("//div[@class='itinerary-page']");
	private By continueAsGuest = By.xpath("//button[@id='countinueGuest']");
	private By guestName = By.xpath("//div[contains(@class,'guest-details__name')]/input");
	private By guestMobile = By.xpath("//div[contains(@class,'guest-details__mobile')]/input");
	private By guestEmail = By.xpath("//div[contains(@class,'guest-details__email')]/input");
	private By guestSpecialRequest = By.xpath("//div[contains(@class,'request-details__specific')]/textarea");
	private By payNowButton = By.xpath("//button[@id='payNow']");
	private By payAtHotelButton = By.xpath("//button[@id='payatHotel']");
	private By soldoutMessage = By.xpath("//div[contains(@class,'soldout-modal')]");
	private By soldoutModalTitle = By.xpath("//div[@class='soldout-modal__title']"); // Oops!
																						// The
																						// hotel
																						// you
																						// picked
																						// is
																						// no
																						// longer
																						// available.
	private By soldoutModalInfo = By.xpath("//div[@class='soldout-modal__info']"); // Please
																					// choose
																					// another
																					// hotel
																					// or
																					// modify
																					// your
																					// travel
																					// dates.
	private By chooseAnotherHotel = By.xpath("//a[text()= 'choose another hotel']");
	private By modifyTravelDates = By.xpath("//a[text()= 'modify travel dates']");

	private By hotelNameInItineraryPage = By.xpath("//div[@class='itinerary-view__hotel-info__name']/a");
	private By hotelAddressInItineraryPage = By.xpath("//div[@class='itinerary-view__hotel-info__address']");
	private By hotelCheckInDateInItineraryPage = By.xpath("//div[contains(@class,'analytics-checkin')]");
	private By hotelCheckOutDateInItineraryPage = By.xpath("//div[contains(@class,'analytics-checkout')]");
	private By guestDetailsInItineraryPage = By
			.xpath("//div[@class='itinerary-view__booking']/div[3]//div[@class='itinerary-view__booking-info']");
	private By roomInfoInItineraryPage = By.xpath(
			"//div[@class='itinerary-view__booking']/div[4]//div[contains(@class,'itinerary-view__booking-info')]/span");
	private By roomTypeInItineraryPage = By
			.xpath("//div[@class='itinerary-view__booking']/div[5]//div[@class='itinerary-view__booking-info']");

	private By roomPrice = By.xpath("//span[@id='roomPrice']");
	private By totalTax = By.xpath("//span[@id='totalTax']");
	private By discountValue = By.xpath("//span[@class='analytics-discountvalue']");
	private By grandTotal = By.xpath("//span[@id='grandTotal']");

	private By applyCouponCodeLink = By.cssSelector(".js-applycoupon");
	private By inputCoupon = By.xpath("//div[@class='apply-screen']//input[contains(@class,'discount__voucher')]");
	private By applyCouponButton = By.xpath("//div[@class='apply-screen']//input[contains(@class,'discount__applybtn')]");
	private By discountAppliedValue = By.cssSelector(".discount__applied-screen:not(.hide) .analytics-discountvalue");
	private By couponApplied = By.cssSelector(".discount__applied-screen:not(.hide) .analytics-coupon");
	private By removeCoupon = By.cssSelector(".discount__applied-screen:not(.hide) .text-right a");
	private By couponInvalidError = By.xpath("//div[@class='apply-screen']//div[@id='discountError']");

	private By guestLoginTreeboEmail = By.xpath("//input[@id='guestLoginEmail']");
	private By guestLoginTreeboPassword = By.xpath("//input[@id='guestLoginPassword']");
	private By guestLoginButton = By.xpath("//button[@id='guestLoginBtn']");

	private By newUserRegisterLink = By.xpath("//span[@id='guestRegister']");

	private By guestLoginFBButton = By.xpath("//button[@id='guestFBLogin']");
	private By fbEmailField = By.xpath("//html[@id='facebook']//input[@id='email']");
	private By fbPasswordField = By.xpath("//html[@id='facebook']//input[@id='pass']");
	private By fbLoginButton = By.xpath("//html[@id='facebook']//input[@name='login']");

	private By guestLoginGoogleButton = By.xpath("//button[@id='guestGoogleLogin']");
	private By gmailEmailField = By.xpath("//input[@id='Email']");
	private By gmailNextLink = By.xpath("//input[@id='next']");
	private By gmailPasswordField = By.xpath("//input[@id='Passwd']");
	private By gmailSignInButton = By.xpath("//input[@id='signIn']");

	private By guestLoggedIn = By.xpath("//div[@id='userLoggedin']//a[@id='changeUser']");

	// payU
	private By ccNumber = By.xpath("//div[@id='credit']//input[@id='ccard_number']");
	private By ccName = By.xpath("//div[@id='credit']//input[@id='cname_on_card']");
	private By cvvNumber = By.xpath("//div[@id='credit']//input[@id='ccvv_number']");
	private By monthSelect = By.xpath("//div[@id='credit']//select[@id='cexpiry_date_month']");
	private By mayMonth = By.xpath("//div[@id='credit']//select[@id='cexpiry_date_month']/option[@value='05']");
	private By YearSelect = By.xpath("//div[@id='credit']//select[@id='cexpiry_date_year']");
	private By yearOption = By.xpath("//div[@id='credit']//select[@id='cexpiry_date_year']/option[@value='2017']");
	private By ccPayButton = By.xpath("//div[@id='credit']//input[@id='pay_button']");

	public void verifyItineraryPagePresence() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(itineraryPage));
		Assert.assertTrue(driver.findElement(itineraryPage).isDisplayed());
		System.out.println("Itinerary Page is Displayed");
	}

	public void clickContinueAsGuest() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("clicked on continue as guest");
		driver.findElement(continueAsGuest).click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void bookHotelAsGuestWithPayAtHotel(String strGuestName, String strGuestMobile, String strGuestEmail) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		homePage = new HomePage();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(guestName));
		driver.findElement(guestName).sendKeys(strGuestName);
		driver.findElement(guestMobile).sendKeys(strGuestMobile);
		driver.findElement(guestEmail).sendKeys(strGuestEmail);
		driver.findElement(payAtHotelButton).click();
		homePage.waitForLoaderToDisappear();
		System.out.println("Booking hotel with details: " + "Guest Name : " + strGuestName + " Guest Mobile : "
				+ strGuestMobile + " Guest Email : " + strGuestEmail);
		Assert.assertFalse(checkIfItineraryPageHasError(), "Booking error");
	}

	public boolean checkIfItineraryPageHasError() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();

		if (browserHelper.getCurrentUrl().contains("error=Unable%20to%20save%20booking")) {
			System.out.println("******* Unable to save booking ***********" + browserHelper.getCurrentUrl());
			return true;
		} else if (browserHelper.getCurrentUrl().contains("error")) {
			System.out.println("********* Some error : *******" + browserHelper.getCurrentUrl());
			return true;
		}

		return false;
	}

	public String[] bookHotelAsGuestWithPayAtHotel() throws FileNotFoundException, IOException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		homePage = new HomePage();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(guestName));
		String[] guestDetails = { utils.getProperty("GuestName"), utils.getProperty("GuestMobile"),
				utils.getProperty("TestBookingAccount") };

		driver.findElement(guestName).sendKeys(guestDetails[0]);
		driver.findElement(guestMobile).sendKeys(guestDetails[1]);
		driver.findElement(guestEmail).sendKeys(guestDetails[2]);
		driver.findElement(payAtHotelButton).click();
		homePage.waitForLoaderToDisappear();
		System.out.println("Booking hotel with details: " + "Guest Name : " + guestDetails[0] + " Guest Mobile : "
				+ guestDetails[1] + " Guest Email : " + guestDetails[2]);
		Assert.assertFalse(checkIfItineraryPageHasError(), "Error displayed");
		return guestDetails;
	}

	public void bookWithPayAtHotel() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		homePage = new HomePage();
		homePage.waitForLoaderToDisappear();
		driver.findElement(payAtHotelButton).click();
		Assert.assertFalse(checkIfItineraryPageHasError(), "Error displayed");
	}

	public void enterGuestDetailsAtItineraryPage(String strGuestName, String strGuestMobile, String strGuestEmail) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(guestName));
		driver.findElement(guestName).clear();
		driver.findElement(guestName).sendKeys(strGuestName);
		driver.findElement(guestMobile).clear();
		driver.findElement(guestMobile).sendKeys(strGuestMobile);
		driver.findElement(guestEmail).clear();
		driver.findElement(guestEmail).sendKeys(strGuestEmail);

	}

	public String[] enterGuestDetailsAtItineraryPage() throws FileNotFoundException, IOException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(guestName));
		String[] guestDetails = { utils.getProperty("GuestName"), utils.getProperty("GuestMobile"),
				utils.getProperty("TestBookingAccount") };

		driver.findElement(guestName).clear();
		driver.findElement(guestName).sendKeys(guestDetails[0]);
		driver.findElement(guestMobile).clear();
		driver.findElement(guestMobile).sendKeys(guestDetails[1]);
		driver.findElement(guestEmail).clear();
		driver.findElement(guestEmail).sendKeys(guestDetails[2]);
		return guestDetails;
	}

	public void enterSpecialRequest(String strSpecialRequest) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(guestSpecialRequest).clear();
		driver.findElement(guestSpecialRequest).sendKeys(strSpecialRequest);
	}

	public void loginAsTreeboMember(String strGuestEmail, String strGuestPassword) throws InterruptedException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(guestLoginTreeboEmail).sendKeys(strGuestEmail);
		driver.findElement(guestLoginTreeboPassword).sendKeys(strGuestPassword);
		driver.findElement(guestLoginButton).click();
		homePage = new HomePage();
		homePage.waitForLoaderToDisappear();
		Thread.sleep(5000);
	}

	public String[] loginAsTreeboMember() throws FileNotFoundException, IOException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		String[] guestDetails = { utils.getProperty("TestBookingAccount"), utils.getProperty("TestLoginPassword") };

		driver.findElement(guestLoginTreeboEmail).sendKeys(guestDetails[0]);
		driver.findElement(guestLoginTreeboPassword).sendKeys(guestDetails[1]);
		driver.findElement(guestLoginButton).click();
		homePage = new HomePage();
		homePage.waitForLoaderToDisappear();
		return guestDetails;
	}

	public void loginAsFBMember(String strFBEmail, String strFBPassword) throws InterruptedException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();

		driver.findElement(guestLoginFBButton).click();
		String parentWindow = browserHelper.switchToNewWindow();
		driver.findElement(fbEmailField).sendKeys(strFBEmail);
		driver.findElement(fbPasswordField).sendKeys(strFBPassword);
		driver.findElement(fbLoginButton).click();
		Thread.sleep(30000);
		driver.switchTo().window(parentWindow);
		homePage = new HomePage();
		homePage.waitForLoaderToDisappear();
	}

	public String[] loginAsFBMember() throws FileNotFoundException, IOException, InterruptedException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		homePage = new HomePage();
		browserHelper = new BrowserHelper();
		String[] guestFBDetails = { utils.getProperty("FBLogin"), utils.getProperty("GmailFbPassword") };

		driver.findElement(guestLoginFBButton).click();
		browserHelper.waitTime(10000);
		String parentWindow = browserHelper.switchToNewWindow();
		driver.findElement(fbEmailField).sendKeys(guestFBDetails[0]);
		driver.findElement(fbPasswordField).sendKeys(guestFBDetails[1]);
		driver.findElement(fbLoginButton).click();
		Thread.sleep(30000);
		driver.switchTo().window(parentWindow);
		homePage.waitForLoaderToDisappear();
		return guestFBDetails;
	}

	public void loginAsGoogleMember(String strGmail, String strGmailPassword) throws InterruptedException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		homePage = new HomePage();
		driver.findElement(guestLoginGoogleButton).click();
		String parentWindow = browserHelper.switchToNewWindow();
		driver.findElement(gmailEmailField).sendKeys(strGmail);
		driver.findElement(gmailNextLink).click();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(gmailPasswordField));
		driver.findElement(gmailPasswordField).sendKeys(strGmailPassword);
		driver.findElement(gmailSignInButton).click();
		Thread.sleep(30000);
		driver.switchTo().window(parentWindow);
		homePage.waitForLoaderToDisappear();
	}

	public String[] loginAsGoogleMember() throws FileNotFoundException, IOException, InterruptedException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		homePage = new HomePage();
		CommonUtils utils = new CommonUtils();
		String[] guestGmailDetails = { utils.getProperty("TestBookingAccount"), utils.getProperty("GmailFbPassword") };

		driver.findElement(guestLoginGoogleButton).click();
		String parentWindow = browserHelper.switchToNewWindow();
		driver.findElement(gmailEmailField).sendKeys(guestGmailDetails[0]);
		driver.findElement(gmailNextLink).click();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(gmailPasswordField));
		driver.findElement(gmailPasswordField).sendKeys(guestGmailDetails[1]);
		driver.findElement(gmailSignInButton).click();
		Thread.sleep(90000);
		driver.switchTo().window(parentWindow);
		homePage.waitForLoaderToDisappear();
		return guestGmailDetails;
	}

	public void signupInItineraryPage(String name, String mobile, String email, String password)
			throws InterruptedException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		homePage = new HomePage();
		driver.findElement(newUserRegisterLink).click();
		homePage.waitForLoaderToDisappear();
		homePage.enterSignUpForm(name, mobile, email, password);
		Thread.sleep(30000);
		homePage.waitForLoaderToDisappear();
	}

	public String[] signupInItineraryPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		homePage = new HomePage();
		driver.findElement(newUserRegisterLink).click();
		homePage.waitForLoaderToDisappear();
		return homePage.enterSignUpForm();
	}

	public void verifyGuestLoggedIn() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(guestLoggedIn));
	}
	
	public boolean isPayUPageDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(ccPayButton));
		return driver.findElement(ccPayButton).isDisplayed();
	}
	
	public boolean isGoBackLinkTreeboDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//a[text()='www.treebohotels.com']")).isDisplayed();
	}
	
	public void clickGoBackToTreeboOnPayU(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath("//a[text()='www.treebohotels.com']")).click();
	}
	
	public void verifyPayUPaymentFailed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(payNowButton));
		Assert.assertEquals(driver.findElement(By.cssSelector(".alert__msg")).getText(),"PayU payment failed","Error not displayed");
	}
	
	public void clickOnPayNowButton(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(payNowButton).click();
	}

	public void bookWithPayNow() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		homePage = new HomePage();
		String strCCNumber = "5123456789012346";
		String strName = "Test";
		String strCVVNumber = "123";

		WebDriverWait wait = new WebDriverWait(driver, 180);
		driver.findElement(payNowButton).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(ccNumber));
		for (char ch : strCCNumber.toCharArray()) {
			driver.findElement(ccNumber).sendKeys(String.valueOf(ch));
		}
		driver.findElement(ccName).sendKeys(strName);
		driver.findElement(cvvNumber).sendKeys(strCVVNumber);
		driver.findElement(monthSelect).click();
		driver.findElement(mayMonth).click();
		driver.findElement(YearSelect).click();
		driver.findElement(yearOption).click();
		driver.findElement(ccPayButton).click();
		homePage.waitForLoaderToDisappear();
		wait.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//div[@class='confirmation-page__header']/div[@class='cp-head']")));
	}

	public double[] getHotelPriceInfoInItineraryPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		double[] hotelPriceInfo = new double[4];
		hotelPriceInfo[0] = Double.parseDouble(driver.findElement(roomPrice).getText());
		hotelPriceInfo[1] = Double.parseDouble(driver.findElement(totalTax).getText());
		try {
			hotelPriceInfo[2] = Double.parseDouble(driver.findElement(discountValue).getText());
		}catch (NumberFormatException e){
			hotelPriceInfo[2] = Double.parseDouble("0");
		}
				
		hotelPriceInfo[3] = Double.parseDouble(driver.findElement(grandTotal).getText());
		System.out.println(" Itinerary Page Price :" + "Room Price : " + hotelPriceInfo[0] + " Total Tax : "
				+ hotelPriceInfo[1] + " Total Discount : " + hotelPriceInfo[2] + " Grand Total :" + hotelPriceInfo[3]);
		return hotelPriceInfo;
	}
	
	public void applyCouponInItineraryPage(String coupon){
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Applying coupon in Itinerary page :" + coupon);
		driver.findElement(applyCouponCodeLink).click();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(inputCoupon));
		driver.findElement(inputCoupon).sendKeys(coupon);
		driver.findElement(applyCouponButton).click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void removeCouponInItineraryPage(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		System.out.println("removing coupon applied");
		WebDriverWait wait = new WebDriverWait(driver, 120);
		driver.findElement(removeCoupon).click();
		browserHelper.waitTime(3000);
	}
	
	public int getCouponDiscountDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		int discountApplied = (int) Math.round(Double.parseDouble(driver.findElement(discountAppliedValue).getText()));
	    System.out.println("Discount coupon applied :" + discountApplied);
		return discountApplied;
	}
	
	public void verifyInvalidCouponError(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Verifying invalid error");
		CommonUtils utils = new CommonUtils();
		String errorText = utils.getProperty("InvalidCouponError");
		Assert.assertTrue(driver.findElement(couponInvalidError).isDisplayed(),"Invalid coupon error is not displayed");
		Assert.assertEquals(driver.findElement(couponInvalidError).getText(),errorText,"Error text not matching");
	}
	
	public String getGuestDetailsInItineraryPage(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(guestDetailsInItineraryPage).getText();
	}
	
	public String getRoomInfoInItineraryPage(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(roomInfoInItineraryPage).getText();
	}
}
