package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class TodaysCheckoutScreen {
  private WebDriver driver;

  private By todaysCheckoutScreenTitle = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/toolbar_title'][@text=\"TODAY'S CHECK OUTS\"]");
  private By navigateUpLink = By.xpath("//android.widget.ImageButton[@content-desc='Navigate up']");
  private By refreshLink = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/refresh_checkin']");
  private By dateRightArrow = By.xpath("//android.widget.ImageView[@resource-id='com.treebo.bumblebee:id/date_arrow_right']");
  private By dateLeftArrow = By.xpath("//android.widget.ImageView[@resource-id='com.treebo.bumblebee:id/date_arrow_left']");
  private By weekViewDate = By.xpath("//android.view.View[@resource-id='com.treebo.bumblebee:id/week_view']");
  private By bookingLogs = By.xpath("//android.support.v7.widget.RecyclerView[@resource-id='com.treebo.bumblebee:id/bookings_log']/android.widget.RelativeLayout");
  private By roomType = By.xpath("//android.widget.RelativeLayout[@resource-id='com.treebo.bumblebee:id/room_details']/android.widget.TextView[@resource-id='com.treebo.bumblebee:id/room_type']");
  private By roomNumber = By.xpath("//android.widget.RelativeLayout[@resource-id='com.treebo.bumblebee:id/room_details']/android.widget.TextView[@resource-id='com.treebo.bumblebee:id/room_number']");
  private By guestDetails = By.xpath("//android.widget.RelativeLayout[@resource-id='com.treebo.bumblebee:id/guest_details_container']/android.widget.TextView[@resource-id='com.treebo.bumblebee:id/guests']");
  private By numberOfNights = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/room_nights']");
  private By pendingAmountTitle = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/title_pending_amount']");
  private By pendingAmountValue = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/text_pending_amount']");
  private By bookingListEmpty = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/booking_list_empty'][@text='There are no checkouts scheduled for this date']");

  private By bookingDetailsDrawer = By.xpath("//android.widget.LinearLayout/android.widget.RelativeLayout[@resource-id='com.treebo.bumblebee:id/drawer_mini_top_row']");
  private By slideUp = By.xpath("//android.widget.ImageView[@resource-id='com.treebo.bumblebee:id/slide_up']");
  private By closeMiniDrawer = By.xpath("//android.widget.ImageView[@resource-id='com.treebo.bumblebee:id/close_drawer']");
  private By checkOutButton = By.xpath("//android.widget.Button[@text='Check Out']");
  private By editButton = By.xpath("//android.widget.Button[@text='Edit Booking']");
  private By extendStay = By.xpath("//android.widget.Button[@text='Extend Stay']");

  private By statusLabel = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/status_label'][@text='Status']");
  private By status = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/status']");

  private By paymentModeLabel = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/payment_mode_label'][@text='Mode of Payment']/");
  private By paymentMode = By.xpath("//android.widget.TextView[resource-id='com.treebo.bumblebee:id/payment_mode']");

  private By specialRequestLabel = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/spec_req_label'][@text='Special Request']");
  private By specialRequest = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/spec_req']");

  private By bookingIdLabel = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/booking_id_label'][@text='Booking Id']");
  private By bookingId = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/booking_id']");

  private By guestDetailsLabel = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/guest_label'][@text='Guests']");
  private By guestDetailsMiniDrawer = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/guest_details']");

  private By bookingSourceType = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/booking_source_type']");

  private By stayDatesLabel = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/stay_dates_label'][@text='Stay Dates']");
  private By stayDates = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/stay_dates']");
  private By nightsCount = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/nights']");

  private By roomsLabel = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/rooms_label'][@text='Rooms']");
  private By roomsCount = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/rooms']");

  private By pendingAmountLabel = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/pending_amount_label'][@text='Pending Amount']");
  private By pendingAmount = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/pending_amount']"); //₹ 0

  public TodaysCheckoutScreen(WebDriver driver) {
    this.driver=driver;
  }

  public void verifyTodaysCheckoutScreenPresence(){
    WebDriverWait wait = new WebDriverWait(driver, 60000);
    wait.until(ExpectedConditions.presenceOfElementLocated(todaysCheckoutScreenTitle));
    Assert.assertTrue(driver.findElement(todaysCheckoutScreenTitle).isDisplayed());
  }

  public void verifyGuestDetailsOnTiles(){
    WebDriverWait wait = new WebDriverWait(driver, 60000);
    wait.until(ExpectedConditions.presenceOfElementLocated(guestDetails));
    Assert.assertTrue(driver.findElement(guestDetails).isDisplayed());
    Assert.assertTrue(driver.findElement(roomType).isDisplayed());
    Assert.assertTrue(driver.findElement(roomNumber).isDisplayed());
    Assert.assertTrue(driver.findElement(numberOfNights).isDisplayed());
    Assert.assertTrue(driver.findElement(pendingAmountTitle).isDisplayed());
    Assert.assertTrue(driver.findElement(pendingAmountValue).isDisplayed());
  }

  public void clickOnNavigateBack(){
    WebDriverWait wait = new WebDriverWait(driver, 60000);
    wait.until(ExpectedConditions.presenceOfElementLocated(navigateUpLink));
    driver.findElement(navigateUpLink).click();
  }

/**
 * This method expects that the bookings are available on next date
 */
  public void navigateToFutureCheckOutsWithBooking(){
    WebDriverWait wait = new WebDriverWait(driver, 60000);
    driver.findElement(dateRightArrow).click();
    wait.until(ExpectedConditions.presenceOfElementLocated(bookingLogs));
  }

  public void clickOnRefreshButtonInTodaysCheckOutScreen(){
    driver.findElement(refreshLink).click();
  }

  public void clickOnFirstCheckoutTile(){
    WebDriverWait wait = new WebDriverWait(driver, 60000);
    wait.until(ExpectedConditions.presenceOfElementLocated(guestDetails));
    driver.findElement(guestDetails).click();
  }

  public void clickOnBookingTileByName(String bookingName){
    WebDriverWait wait = new WebDriverWait(driver, 60000);
    String bookingNameXpath = String.format("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/guests'][@text='%s']",bookingName);
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(bookingNameXpath)));
    driver.findElement(By.xpath(bookingNameXpath)).click();
  }

  public void verifyCheckOutButtonStatusIsDisabled(){
    WebDriverWait wait = new WebDriverWait(driver, 60000);
    wait.until(ExpectedConditions.presenceOfElementLocated(checkOutButton));
     Assert.assertTrue(!driver.findElement(checkOutButton).isEnabled());
  }

  public void clickOnCheckoutButton(){
    WebDriverWait wait = new WebDriverWait(driver, 60000);
    wait.until(ExpectedConditions.presenceOfElementLocated(checkOutButton));
    Assert.assertTrue(driver.findElement(checkOutButton).isEnabled());
    driver.findElement(checkOutButton).click();
  }

}
