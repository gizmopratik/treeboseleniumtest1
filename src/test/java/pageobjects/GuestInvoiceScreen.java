package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class GuestInvoiceScreen {
  private WebDriver driver;

  private By guestInvoiceScreenTitle = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/toolbar_title'][@text='Guest Invoice']");
  private By invoiceBillToLabel = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/label'][@text='Bill To -']");
  private By invoiceBillToGuestName = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/value']");

  private By balanceAmountInInvoice = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/balance_amount']");

  public GuestInvoiceScreen(WebDriver driver) {
    this.driver=driver;
  }

  public void verifyGuestInvoiceScreenPresence(){
    WebDriverWait wait = new WebDriverWait(driver, 120000);
    wait.until(ExpectedConditions.presenceOfElementLocated(guestInvoiceScreenTitle));
    Assert.assertTrue(driver.findElement(guestInvoiceScreenTitle).isDisplayed());
  }

  public String returnGuestNameInInvoice(){
    WebDriverWait wait = new WebDriverWait(driver, 120000);
    wait.until(ExpectedConditions.presenceOfElementLocated(invoiceBillToLabel));
    Assert.assertTrue(driver.findElement(invoiceBillToLabel).isDisplayed());
    Assert.assertTrue(driver.findElement(invoiceBillToGuestName).isDisplayed());
    return driver.findElement(invoiceBillToGuestName).getText();
  }

  public void verifyBalanceAmountInInvoiceIsZero(){
    WebDriverWait wait = new WebDriverWait(driver, 120000);
    wait.until(ExpectedConditions.presenceOfElementLocated(balanceAmountInInvoice));
    Assert.assertTrue(driver.findElement(balanceAmountInInvoice).isDisplayed());
    Assert.assertTrue(driver.findElement(balanceAmountInInvoice).getText().split(" ")[1].equals("0"));
  }
}
