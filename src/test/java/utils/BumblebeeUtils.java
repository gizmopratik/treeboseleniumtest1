package utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;



public class BumblebeeUtils {
	public static String getCheckinDate(){
		java.util.Date dateNow = new Date();
		DateTime dt = new DateTime(dateNow);
		int month = dt.getMonthOfYear();
		int year = dt.getYear();
		int day = dt.getDayOfMonth();
		return year+"-"+month+"-"+day;
	}

	public static String getCheckoutDate(){
		java.util.Date dateNow = new Date();
		DateTime dt = new DateTime(dateNow);
		int month = dt.getMonthOfYear();
		int year = dt.getYear();
		int day = dt.getDayOfMonth();
		day+=1;
		return year+"-"+month+"-"+day;
	}

	

	public static String makeString(String api){
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = Thread.currentThread().getContextClassLoader().getResourceAsStream("bumblebeeTest.properties");

			// load a properties file
			prop.load(input);

			// get the property value and print it out




		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		// Get the url from POM, url, if its present if not set the default
		//
		String SysURL = System.getProperty("url");
		if(SysURL == null){
			String url = prop.getProperty("staging");
			return url+prop.getProperty(api);
		}
		else{
			return SysURL+prop.getProperty(api);
		}
	}

	public static String getFromProperty(String property){
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = Thread.currentThread().getContextClassLoader().getResourceAsStream("bumblebeeTest.properties");

			// load a properties file
			prop.load(input);

			// get the property value and print it out




		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return prop.getProperty(property);
	}
	
	public static String getToken() throws ClientProtocolException, IOException, ParseException, JSONException {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(BumblebeeUtils.makeString("login"));

		try{

			EntityBuilder builder = EntityBuilder.create();
			List<NameValuePair> data = new ArrayList<NameValuePair>();
			data.add(new BasicNameValuePair("username", getFromProperty("email")));
			data.add(new BasicNameValuePair("password", getFromProperty("password")));
			data.add(new BasicNameValuePair("hotel_id", getFromProperty("hotelID")));
			builder.setParameters(data);
			HttpEntity entity = builder.build();
			httpPost.setEntity(entity);

			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity responseEntity = response.getEntity();
			JSONObject jsonObject = new JSONObject(EntityUtils.toString(responseEntity));
			return  jsonObject.getJSONObject("data").getString("access_token");
		}
		finally{
			httpClient.close();
		}

	}

		public static String getAvailability(){
			Properties prop = new Properties();
			InputStream input = null;
	
			try {
	
				input = Thread.currentThread().getContextClassLoader().getResourceAsStream("bumblebeeTest.properties");
	
				// load a properties file
				prop.load(input);
	
				// get the property value and print it out
			} catch (IOException ex) {
				ex.printStackTrace();
			} finally {
				if (input != null) {
					try {
						input.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			if(makeString("availability").contains("staging")){
			return makeString("availability")+"checkin_date="+getCheckinDate()+"&checkout_date="+getCheckoutDate()+"&adult="+prop.getProperty("adult")+"&child="+prop.getProperty("child")+"&no_of_rooms="+prop.getProperty("noOfRooms")+"&hotel_id="+prop.getProperty("hotelID")+"&user_id="+prop.getProperty("userID");
			}
			else{
				return makeString("availability")+"checkin_date="+getCheckinDate()+"&checkout_date="+getCheckoutDate()+"&adult="+prop.getProperty("adult")+"&child="+prop.getProperty("child")+"&no_of_rooms="+prop.getProperty("noOfRooms")+"&hotel_id="+prop.getProperty("prodHotelID")+"&user_id="+prop.getProperty("userID");
			}
		}

	public static void main(String[] args) throws ClientProtocolException, IOException{
				
				System.out.println(makeString("availability"));
	}

}


