package PageFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.security.SecureRandom;


public class NewUserRegistrationPage {

	
    WebDriver driver;
    
    @FindBy(name="headerRegister")
    WebElement headerRegister;

    @FindBy(name="name")
    WebElement name;
    
    @FindBy(name="mobile")
    WebElement mobile;
    
    @FindBy(id="signupEmail")
    WebElement email;
    
    @FindBy(id="signuppassword")
    WebElement password;

    @FindBy(name="popupSignUp")
    WebElement signUpButton;
    
    
    public NewUserRegistrationPage(WebDriver driver){
        this.driver = driver;
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }
    
    
    public void setName(String strName){
    	name.sendKeys(strName);
    }
 
    public void setMobile(String strMobile){
    	mobile.sendKeys(strMobile);
    }
    
    public void setEmail(String strEmail){
    	email.sendKeys(strEmail);
    }
    
    public void setPassword(String strPassword){
    	password.sendKeys(strPassword);
    } 
    
    //Click on Singup button
    public void clickSignupButtton(){
    	signUpButton.click();
    }
    
    public  String generateRandomEmail(){
    	Random RANDOM = new SecureRandom();
    	String letters = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789+@";
    	String emailId = "";
    	int emailIdLength = 10;
    	
        for (int i=0; i<emailIdLength; i++)
        {
            int index = (int)(RANDOM.nextDouble()*letters.length());
            emailId += letters.substring(index, index+1);
        }
        return emailId +"@test.treebohotels.com";
    	
    }
    
    
    public void createNewUser(String strName, String strMobile, String strEmail,String strPassword){
    	// If popup is not present, click on headerLogin.
    	if(driver.findElements(By.name("emailId") ).size() == 0)
    	{
    		headerRegister.click();
    	}
    	
    	driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
    	ExpectedConditions.visibilityOf(email);
    	
        //Fill user name
        this.setName(strName);
 
        //Fill Mobile Number
        this.setMobile(strMobile);
        
        //Fill Email Id
        this.setEmail(strEmail);
 
        //Fill password
        this.setPassword(strPassword);
       
        //Click Login button
        this.clickSignupButtton();
    	
    }
    
    
    
}
