package bumblebee;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import utils.BumblebeeUtils;

public class AvailablilityTest{
	@Test(groups = {"availability", "bumblebee"})
	public void availabilityBasicTest() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getAvailability());
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());
		
		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		finally{
			httpClient.close();
		}
	}
	
	@Test(groups = {"availability", "bumblebee"})
	public void responseTest() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getAvailability());
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());
		
		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();
			
			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));
			
			for(int i = 0; i<jsonObject.getJSONObject("data").getJSONArray("rooms").length(); i++){
			Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(0).getJSONArray("prices").getJSONObject(0).getInt("price")>0);
			Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(0).getJSONArray("prices").getJSONObject(0).getInt("tax")>0);
			Assert.assertTrue(Integer.parseInt(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getString("max_occupancy"))>0);
			}
			System.out.println(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(0).getString("max_occupancy"));

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}
}
